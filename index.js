const fetch = require('node-fetch');

module.exports = async (request, response) => {

  response.setHeader('Access-Control-Allow-Origin', '*')

  let url = process.env.DATA_URL || 'http://localhost:5001';

  console.log("distribution-service DATA_URL: ", url);

	const res = await fetch(url);
	const objects = await res.json();


  let total_points = 0;
  objects.forEach(function(object){
    total_points = object.points + total_points;
  })

  objects.forEach(function(object){
    object.share = object.points / total_points;
  })

	return objects;
}
